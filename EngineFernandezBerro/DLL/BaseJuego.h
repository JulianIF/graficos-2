#pragma once
#include "Export.h"
#include "Renderer.h"
#include "Ventana.h"
#include "TypeDef.h"
#include <iostream>
using namespace std;

class DLL_API BaseJuego
{
public:
	BaseJuego();
	~BaseJuego();
	bool Inicio();
	bool Stop();
	void Loop();
private:
	Ventana* ventana;
protected:
	Renderer * render;
	virtual bool OnStart() = 0;
	virtual bool OnStop() = 0;
	virtual bool OnUpdate() = 0;
	virtual void OnDraw() = 0;
};

