#include "LoaderTexturas.h"


LoaderTexturas::LoaderTexturas()
{
}


LoaderTexturas::~LoaderTexturas()
{
}

Header LoaderTexturas::CargaBMP(const char * imagepath)
{
	unsigned char header[54]; // Each BMP file begins by a 54-bytes header

	Header bmp;

	// Apertura del archivo
	FILE * file;
	fopen_s(&file, imagepath, "rb");

	if (Format(header, file))
	{
		// Lectura de los enteros desde el arreglo de bytes
		bmp.dataPos = *(int*)&(header[0x0A]);
		bmp.imageSize = *(int*)&(header[0x22]);
		bmp.width = *(int*)&(header[0x12]);
		bmp.height = *(int*)&(header[0x16]);
	}

	// Algunos archivos BMP tienen un mal formato, as� que adivinamos la informaci�n faltante
	if (bmp.imageSize == 0)    
		bmp.imageSize = bmp.width * bmp.height * 3; // 3 : un byte por cada componente Rojo (Red), Verde (Green) y Azul(Blue)
	if (bmp.dataPos == 0)      
		bmp.dataPos = 54; // El encabezado del BMP est� hecho de �sta manera

	// Se crea un buffer
	bmp.data = new unsigned char[bmp.imageSize];

	fseek(file, bmp.dataPos, 0);
	// Leemos la informaci�n del archivo y la ponemos en el buffer
	fread(bmp.data, 1, bmp.imageSize, file);

	//Todo est� en memoria ahora, as� que podemos cerrar el archivo
	fclose(file);
	return bmp;
}

bool LoaderTexturas::Format(unsigned char header[], FILE * file)
{
	if (!file)
	{
		printf("Image could not be opened\n"); return 0;
	}

	if (fread(header, 1, 54, file) != 54)
	{
		// If not 54 bytes read : problem
		printf("Not a correct BMP file\n");
		return false;
	}

	if (header[0] != 'B' || header[1] != 'M')
	{
		printf("Not a correct BMP file\n");
		return 0;
	}
}

