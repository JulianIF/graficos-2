#pragma once
#include "Export.h"
#include <glm\glm.hpp>
#include <GL\glew.h>
#include <glm\gtc\matrix_transform.hpp>
#include <stdio.h>
struct Header
{
	unsigned int dataPos;
	unsigned int width, height;
	unsigned int imageSize;
	unsigned char  * data;
};
static class DLL_API LoaderTexturas
{
public:
	LoaderTexturas();
	~LoaderTexturas();
	Header CargaBMP(const char * imagepath);
	bool Format(unsigned char header[], FILE * file);
	GLuint SetTexture(Header bmp);
};

