#include "Entidad.h"



Entidad::Entidad(Renderer * rendererPTR): 
	render(rendererPTR),
	matrizMundo (glm::mat4(1.0f)),
	matrizTraslacion (glm::mat4(1.0f)),
	matrizRotacion(glm::mat4(1.0f)),
	matrizEscala(glm::mat4(1.0f))
{
	posicion[0] = posicion[1] = posicion[2] = 0.0f;
	rotacion[0] = rotacion[1] = rotacion[2] = 0.0f;
	escala[0] = escala[1] = escala[2] = 1.0f;
}


Entidad::~Entidad()
{
}

void Entidad::SetPosicion(float x, float y, float z)
{
	posicion[0] = x;
	posicion[1] = y;
	posicion[2] = z;

	matrizTraslacion = glm::translate(glm::mat4(1.0f), posicion);
	UpdateMatrizDeMundo();
}

void Entidad::SetRotacion(float x, float y, float z)
{
	rotacion[0] = x;
	rotacion[1] = y;
	rotacion[2] = z;

	matrizRotacion = glm::rotate(glm::mat4(1.0f), x, glm::vec3(1.0f, 0.0f, 0.0f));
	matrizRotacion = glm::rotate(glm::mat4(1.0f), x, glm::vec3(0.0f, 1.0f, 0.0f));
	matrizRotacion = glm::rotate(glm::mat4(1.0f), x, glm::vec3(0.0f, 0.0f, 1.0f));
	UpdateMatrizDeMundo();
}

void Entidad::SetEscala(float x, float y, float z)
{
	escala[0] = x;
	escala[1] = y;
	escala[2] = z;

	matrizEscala = glm::scale(glm::mat4(1.0f), escala);

	UpdateMatrizDeMundo();
}

void Entidad::UpdateMatrizDeMundo()
{
	matrizMundo = matrizTraslacion * matrizRotacion * matrizEscala;
}

glm::vec3 Entidad::GetPosicion()
{
	return posicion;
}
glm::vec3 Entidad::GetRotacion()
{
	return rotacion;
}
glm::vec3 Entidad::GetEscala()
{
	return escala;
}