#pragma once
#include "Export.h"
#include "Ventana.h"
#include<glm\glm.hpp>
#include<glm\gtc\matrix_transform.hpp>
#include <iostream>
using namespace std;

class DLL_API Renderer
{
public:
	Renderer();
	~Renderer();
	bool Inicio(Ventana *ventanaPTR);
	bool Stop();
	//PANTALLA
	void LimpiarPantalla();
	void LimpiarAColor(float r, float g, float b, float a);
	//BUFFERS
	void SwapBuffers();
	unsigned int GenerarBuffer(float* buffer, int tama�o);
	unsigned int GenerarTextureBuffer(int width, int height, unsigned char * data);
	void BindBuffer(unsigned int vtxbuffer, unsigned int nombre);
	void BindUVBuffer(unsigned int clrbuffer, unsigned int nombre);
	void DibujarBuffer(int tama�o, int primitiva);
	void DestruirBuffer(unsigned int buffer);
	//DIBUJADO
	void EmpezarDibujado(unsigned int nombre);
	void TerminarDibujado(unsigned int nombre);
	void UpdateMVP();
	//GETTERS
	glm::mat4& GetMVP();
	//FUNCIONES-MATRICES
	void CargarMatrizIdentidad();
	void SetMatrizDeMundo(glm::mat4 mat);
	void MultiplicarMatrizDeMundo(glm::mat4 mat);
private:
	unsigned int VertexArrayID;
	Ventana * ventana;
	//MATRICES
	glm::mat4 matrizMundo;
	glm::mat4 matrizVista;
	glm::mat4 matrizProyeccion;
	glm::mat4 mvp;
};

