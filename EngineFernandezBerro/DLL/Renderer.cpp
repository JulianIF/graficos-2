#include "Renderer.h"
#include <GL\glew.h>
#include <GLFW\glfw3.h>


Renderer::Renderer()
{
}


Renderer::~Renderer()
{
}


bool Renderer::Inicio(Ventana * windowPTR) 
{

	if (windowPTR != NULL)
	{
		ventana = windowPTR;
		glfwMakeContextCurrent((GLFWwindow*)ventana->GetVentana());

		if (glewInit() != GLEW_OK) {
			cout << "Fall� al inicializar GLEW\n" << endl;
			return -1;
		}

		glGenVertexArrays(1, &VertexArrayID);
		glBindVertexArray(VertexArrayID);

		//Inicializo proyeccion.
		matrizProyeccion = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, 0.0f, 100.f);
		//Inicializo vista.
		matrizVista = glm::lookAt
		(
			glm::vec3(0, 0, 3),
			glm::vec3(0, 0, 0),
			glm::vec3(0, 1, 0)
		);
		//Inicializo mundo.
		matrizMundo = glm::mat4(1.0f);

		UpdateMVP();

		cout << "Inicio Renderer" << endl;
		return true;
	}
	return false;
}

bool Renderer::Stop() 
{
	cout << "Stop Renderer" << endl;
	return false;
}
//LIMPIAR PANTALLA
void Renderer::LimpiarPantalla()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::LimpiarAColor(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a);
}
//BUFFERS
unsigned int Renderer::GenerarBuffer(float* _buffer, int size)
{
	unsigned int buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, size, _buffer, GL_STATIC_DRAW);
	return buffer;
}

unsigned int Renderer::GenerarTextureBuffer(int width, int height, unsigned char* data)
{
	// Se Crea una textura OpenGL
	unsigned int  texturebuffer;
	glGenTextures(1, &texturebuffer);

	// Se "Ata" la nueva textura : Todas las futuras funciones de texturas van a modificar esta textura
	glBindTexture(GL_TEXTURE_2D, texturebuffer);

	// Se le pasa la imagen a OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	return texturebuffer;
}
void Renderer::SwapBuffers()
{
	glfwSwapBuffers((GLFWwindow*)ventana->GetVentana());
}

void Renderer::DibujarBuffer(int size, int primitiva)
{
	switch (primitiva)
	{
		case 0:
			glDrawArrays(GL_TRIANGLE_STRIP, 0, size);
			break;
		case 1:
			glDrawArrays(GL_TRIANGLE_FAN, 0, size);
			break;
		default:
			glDrawArrays(GL_TRIANGLES, 0, size);
			break;
	}
}

void Renderer::BindBuffer(unsigned int vtxbuffer, unsigned int atribId)
{
	glBindBuffer(GL_ARRAY_BUFFER, vtxbuffer);
	glVertexAttribPointer(
		atribId,														// Le paso la ubicacion de donde se guardo la mempria del vertice
		3,																// tama�o
		GL_FLOAT,														// tipo
		GL_FALSE,														// normalizado?
		0,																// Paso
		(void*)0														// desfase del buffer
	);
}

void Renderer::BindUVBuffer(unsigned int UVbuffer, unsigned int atribId)
{
	glBindBuffer(GL_ARRAY_BUFFER, UVbuffer);
	glVertexAttribPointer(
		atribId,														// Le paso la ubicacion de donde se guardo la mempria del vertice
		2,																// tama�o
		GL_FLOAT,														// tipo
		GL_FALSE,														// normalizado?
		0,																// Paso
		(void*)0														// desfase del buffer
	);
}

void Renderer::DestruirBuffer(unsigned int buffer)
{
	glDeleteBuffers(1, &buffer);
}
//DIBUJADO
void Renderer::EmpezarDibujado(unsigned int atribId)
{
	glEnableVertexAttribArray(atribId);
}
void Renderer::TerminarDibujado(unsigned int atribId)
{
	glDisableVertexAttribArray(atribId);
}
//MATRIZ IDENTIDAD
void Renderer::CargarMatrizIdentidad()
{
	matrizMundo = glm::mat4(1.0f);
}
//MATRIZ DE MUNDO
void Renderer::MultiplicarMatrizDeMundo(glm::mat4 mat)
{
	matrizMundo *= mat;
	UpdateMVP();
}
void Renderer::SetMatrizDeMundo(glm::mat4 mat)
{
	matrizMundo = mat;
	UpdateMVP();
}
//MVP
void Renderer::UpdateMVP()
{
	mvp = matrizProyeccion * matrizVista * matrizMundo;
}
glm::mat4& Renderer::GetMVP()
{
	return mvp;
}