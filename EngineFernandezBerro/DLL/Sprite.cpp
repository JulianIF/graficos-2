#include "Sprite.h"



Sprite::Sprite(Renderer *render) :FormaColoreada(render)
{
	primitiva = 0;
	verticesUV = NULL;
	UVBufferId = -1;
	textureBufferId = -1;

	vertices = new float[12]
	{
		-1.0f,-1.0f , 0.0f ,
		 1.0f,-1.0f , 0.0f ,
		-1.0f, 1.0f , 0.0f ,
		 1.0f, 1.0f , 0.0f
	};

	SetVertices(vertices, 4);

	verticesUV = new float[8]
	{
		1.0f,  1.0f,  
		0.0f,  1.0f,  
		1.0f,  0.0f,
		0.0f,  0.0f
	};

	SetTexture(verticesUV, 4);
}


Sprite::~Sprite()
{
	Dispose();
}

void Sprite::SetImage(const char* imagepath)
{
	Header h = loader.CargaBMP(imagepath);
	textureBufferId = render->GenerarTextureBuffer(h.width,h.height,h.data);
}

void Sprite::Draw()
{
	render->CargarMatrizIdentidad();
	render->SetMatrizDeMundo(matrizMundo);

	if (material != NULL) 
	{
		material->Bind();
		material->SetMatrizProperty("WVP", render->GetMVP());
		material->BindTexture("myTextureSampler", textureBufferId);
	}

	render->EmpezarDibujado(0);
	render->EmpezarDibujado(1);
	render->BindBuffer(IDBuffer, 0);
	render->BindUVBuffer(IDBufferUV, 1);
	render->DibujarBuffer(countVertices, primitiva);
	render->TerminarDibujado(0);
	render->TerminarDibujado(1);
}

void Sprite::SetAnimation(int initF, int finalF, float timePerF)
{
	anim->SetAnimation(initF, finalF, timePerF);
}

void Sprite::UpdateAnimation(float deltaTime)
{
	verticesUV = anim->UpdateAnimation(deltaTime);
	SetTexture(verticesUV, 4);
}