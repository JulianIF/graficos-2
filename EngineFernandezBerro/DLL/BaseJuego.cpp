#include "BaseJuego.h"



BaseJuego::BaseJuego()
{
}


BaseJuego::~BaseJuego()
{
}

bool BaseJuego::Inicio()
{
	cout << "Inicio" << endl;

	ventana = new Ventana();
	if (!ventana->Start(800, 600, "Fernadez Berro Julian"))
		return false;

	render = new Renderer();
	if (!render->Inicio(ventana))
		return false;
	render->LimpiarAColor(0.0f, 0.0f, 4.0f, 0.0f);
	return OnStart();
}

void BaseJuego::Loop()
{
	bool loop = true;
	while (loop && !ventana->ShouldClose()) 
	{
		loop = OnUpdate();
		render->LimpiarPantalla();
		OnDraw();
		render->SwapBuffers();
		ventana->PollEvents();
	}
}

bool BaseJuego::Stop()
{
	cout << "Stop" << endl;
	OnStop();
	render->Stop();
	ventana->Stop();

	delete render;
	delete ventana;
	return true;
}