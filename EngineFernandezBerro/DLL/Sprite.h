#pragma once
#include "FormaColoreada.h"
#include "LoaderTexturas.h"
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "Animation.h"

class DLL_API Sprite :
	public FormaColoreada
{
public:
	Sprite(Renderer* render);
	~Sprite();
	void SetImage(const char * imagepath);
	void Draw() override;
	Animation  *anim;
	void SetAnimation(int initF, int finalF, float timePerF);
	void UpdateAnimation(float deltaTime);
private:
	float* verticesUV;
	unsigned int textureBufferId;
	unsigned int UVBufferId;
	int textreVtxCount;
	LoaderTexturas loader;
};

