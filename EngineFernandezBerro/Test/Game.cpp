#include "Game.h"



Game::Game() : 
	i(0),
	timer (0),
	j(0)
{
}


Game::~Game()
{
}

bool Game::OnStart() 
{
	mat1 = new Material();
	mat2 = new Material();
	mat3 = new Material();
	manager = new CollisionManager();

	unsigned int programID1 = mat1->CargaShaders("TextureVertexShader.txt", "TextureFragmentShader.txt");
	unsigned int programID2 = mat2->CargaShaders("TextureVertexShader.txt", "TextureFragmentShader.txt");
	unsigned int programID3 = mat3->CargaShaders("ColorVertexShader.txt", "ColorFragmentShader.txt");
	
	t1 = new Triangulo(render);
	r1 = new Rectangulo(render);

	t1->SetMaterial(mat3);
	r1->SetMaterial(mat3);

	
	spr1 = new Sprite(render);
	spr1->anim = new Animation(10, 1);
	spr2 = new Sprite(render);
	spr3 = new Sprite(render);
	
	spr1->SetMaterial(mat1);
	spr2->SetMaterial(mat2);
	spr3->SetMaterial(mat1);

	spr1->SetImage("character.bmp");
	spr2->SetImage("bolitas.bmp");
	spr3->SetImage("character.bmp");

	spr1->SetAnimation(0, 9, 0.5f);

	spr1->collider.isTrigger = false;
	spr2->collider.isTrigger = false;
	spr3->collider.isTrigger = false;

	spr1->collider.height = 2;
	spr1->collider.width = 2;
	spr2->collider.height = 2;
	spr2->collider.width = 2;
	spr3->collider.height = 2;
	spr3->collider.width = 2;

	manager->AddToLayer('A', spr1);
	manager->AddToLayer('B', spr2);
	manager->AddToLayer('A', spr3);

	cout << "Inicio Game" << endl;
	return true;
}

bool Game::OnStop() 
{
	cout << "Stop Game" << endl;
	delete spr1;
	delete spr2;
	delete spr3;
	delete t1;
	delete r1;
	delete manager;
	delete mat1;
	delete mat2;
	delete mat3;
	return false;
}

bool Game::OnUpdate() 
{
	timer += 1;
	j += 0.1f;
	IdaYVuelta(timer);
	spr1->SetPosicion(i, spr1->GetPosicion().y, spr1->GetPosicion().z);
	spr2->SetPosicion(spr2->GetPosicion().x, spr2->GetPosicion().y, spr2->GetPosicion().z);
	spr3->SetPosicion(spr3->GetPosicion().x, i, spr3->GetPosicion().z);
	spr1->UpdateAnimation(0.05f);
	t1->SetPosicion(5, 0, 0);
	r1->SetPosicion(0, -5, 0);
	manager->CheckCollisions();
	if (i <= 1)
		cout << i << endl;
	cout << "Update Game" << i << endl;
	return true;
}

void Game::OnDraw()
{
	spr1->Draw();
	spr2->Draw();
	spr3->Draw();
	t1->Draw();
	r1->Draw();
	cout << "Dibujado Game" << i << endl;
}

void Game::IdaYVuelta(int  &t)
{
	if (t <= 50)
		i += 0.1f;
	else if (t > 50 && t < 101)
		i -= 0.1f;
	else if (t == 101)
		t = 0;
}