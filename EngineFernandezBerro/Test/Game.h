#include "../DLL/Triangulo.h"
#include "../DLL/Rectangulo.h"
#include "../DLL/Material.h"
#include "../DLL/BaseJuego.h"
#include "../DLL/Circulo.h"
#include "../DLL//Sprite.h"
#include "../DLL/CollisionManager.h"
#include <iostream>

class Game : public BaseJuego
{
public:
	Game();
	~Game();
protected:
	bool OnStart() override;
	bool OnStop() override;
	bool OnUpdate() override;
	void OnDraw() override;
private:
	float i;
	float j;
	int timer;
	Sprite * spr1;
	Sprite * spr2;
	Sprite * spr3;
	Rectangulo * r1;
	Triangulo  * t1;
	CollisionManager * manager;
	Material * mat1;
	Material * mat2;
	Material * mat3;
	void IdaYVuelta(int  &t);
};

